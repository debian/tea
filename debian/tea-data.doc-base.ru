Document: tea.ru
Title: Duck Under The Cover: TEA Manual
Author: Peter Semiletov <peter.semiletov@gmail.com>
Abstract: This manual describes what TEA is and how it can be used. 
Section: Editors

Format: HTML
Index: /usr/share/doc/tea-data/ru.html
Files: /usr/share/doc/tea-data/ru.html

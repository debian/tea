tea (63.1.0-2) unstable; urgency=medium

  * Update manpage and convert to AsciiDoc. Closes: #1083003
  * Update d/copyright

 -- Dr. Tobias Quathamer <toddy@debian.org>  Mon, 30 Sep 2024 10:07:27 +0200

tea (63.1.0-1) unstable; urgency=medium

  * New upstream version 63.1.0
    - Refresh patch
    - Mark patch as not needing to be forwarded
  * Change Build-Depends from pkg-config to pkgconf
  * Update Standards-Version to 4.7.0, no changes needed

 -- Dr. Tobias Quathamer <toddy@debian.org>  Thu, 22 Aug 2024 22:12:09 +0200

tea (62.0.2-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Wrap long lines in changelog entries: 61.2.0-1.
    Changes-By: lintian-brush
    Fixes: lintian: debian-changelog-line-too-long
    See-also: https://lintian.debian.org/tags/debian-changelog-line-too-long.html
  * Set upstream metadata fields: Repository.
    Changes-By: lintian-brush
    Fixes: lintian: upstream-metadata-missing-repository
    See-also: https://lintian.debian.org/tags/upstream-metadata-missing-repository.html

  [ Dr. Tobias Quathamer ]
  * New upstream version 62.0.2

 -- Dr. Tobias Quathamer <toddy@debian.org>  Mon, 06 Feb 2023 13:53:31 +0100

tea (61.2.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
    Changes-By: lintian-brush
    Fixes: lintian: upstream-metadata-file-is-missing
    See-also:
    https://lintian.debian.org/tags/upstream-metadata-file-is-missing.html
    Fixes: lintian: upstream-metadata-missing-bug-tracking
    See-also:
    https://lintian.debian.org/tags/upstream-metadata-missing-bug-tracking.html
  * Avoid explicitly specifying -Wl,--as-needed linker flag.
    Changes-By: lintian-brush
    Fixes: lintian: debian-rules-uses-as-needed-linker-flag
    See-also:
    https://lintian.debian.org/tags/debian-rules-uses-as-needed-linker-flag.html

  [ Dr. Tobias Quathamer ]
  * New upstream version 61.2.0
  * Add new patch to enable build options in CMakeLists.txt
  * Remove unneeded custom dh_auto_configure rule

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sun, 06 Nov 2022 15:16:38 +0100

tea (61.1.0-2) unstable; urgency=medium

  * Use cmake instead of qmake, as suggested by upstream
  * Add installation hints
  * Drop patch for qmake (no longer used to build the package)

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sun, 30 Oct 2022 16:55:34 +0100

tea (61.1.0-1) unstable; urgency=medium

  * New upstream version 61.1.0
    - Refresh patch
    - Drop Build-Depends on libqt5qml5 and libqt5quick5
    - Use README.md as documentation
  * Add gbp.conf
  * Update Standards-Version to 4.6.1, no changes needed
  * Use debhelper v13
  * Update d/copyright
  * Add lintian overrides for two false positive errors

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sun, 30 Oct 2022 16:12:45 +0100

tea (50.0.4-1) unstable; urgency=medium

  * New upstream version 50.0.4
  * Update Standards-Version to 4.5.0, no changes needed
  * Support cross building.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: #922535)

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sun, 05 Apr 2020 23:48:34 +0200

tea (47.1.0-1) unstable; urgency=medium

  * New upstream version 47.1.0
  * Update d/copyright
  * Switch to debhelper-compat and use v12
  * Add Rules-Requires-Root: no
  * Update Standards-Version to 4.4.0, no changes needed
  * Apply fixes from cme fix dpkg

 -- Dr. Tobias Quathamer <toddy@debian.org>  Fri, 30 Aug 2019 14:27:41 +0200

tea (47.0.1-1) unstable; urgency=medium

  * New upstream version 47.0.1
  * Update d/watch to point to github

 -- Dr. Tobias Quathamer <toddy@debian.org>  Fri, 07 Dec 2018 16:32:43 +0100

tea (46.3.0-1) unstable; urgency=medium

  * New upstream version 46.3.0

 -- Dr. Tobias Quathamer <toddy@debian.org>  Fri, 19 Oct 2018 23:26:09 +0200

tea (46.0.0-1) unstable; urgency=medium

  * New upstream version 46.0.0
  * Update d/copyright and add two omitted files
  * Use qmake buildsystem explicitly, otherwise debhelper chooses cmake.

 -- Dr. Tobias Quathamer <toddy@debian.org>  Wed, 19 Sep 2018 21:07:16 +0200

tea (45.0.2-1) unstable; urgency=medium

  * New upstream version 45.0.2
  * Update d/copyright
  * Use version 4 for d/watch
  * Update Standards-Version to 4.2.1, no changes needed

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sat, 01 Sep 2018 23:24:50 +0200

tea (44.1.1-2) unstable; urgency=medium

  * Use debhelper v11
  * Update d/copyright
  * Switch Vcs-URLs to salsa.d.o
  * Update Standards-Version to 4.1.3, no changes needed

 -- Dr. Tobias Quathamer <toddy@debian.org>  Mon, 22 Jan 2018 19:23:12 +0100

tea (44.1.1-1) unstable; urgency=medium

  * New upstream version 44.1.1
    - Remove patch, has been applied upstream
  * Use HTTPS for upstream URLs
  * Use wrap-and-sort
  * Update Standards-Version to 4.1.1, no changes needed

 -- Dr. Tobias Quathamer <toddy@debian.org>  Fri, 24 Nov 2017 14:30:15 +0100

tea (44.1.0-1) unstable; urgency=medium

  * New upstream version 44.1.0
    - Refresh patch, mostly applied upstream

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sun, 16 Jul 2017 21:12:32 +0200

tea (44.0.0-1) unstable; urgency=medium

  * New upstream version 44.0.0
    - Remove patch for qmake .pro file, applied upstream
    - Add new patch for desktop file
  * Update d/copyright
  * Update Standards-Version to 4.0.0
    - Use HTTPS for copyright-format URL in d/copyright

 -- Dr. Tobias Quathamer <toddy@debian.org>  Wed, 21 Jun 2017 12:16:28 +0200

tea (43.1.0-1) unstable; urgency=medium

  * Imported Upstream version 43.1.0
  * Refresh patch

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sat, 17 Sep 2016 08:14:15 +0200

tea (43.0.0-1) unstable; urgency=medium

  * Imported Upstream version 43.0.0
  * Update patch
  * Add Multi-Arch: foreign to tea-data
  * Add support for reading PDF and DjVu files
  * Use C.UTF-8 locale for building to support UTF-8 filename
  * Switch to debhelper v10

 -- Dr. Tobias Quathamer <toddy@debian.org>  Fri, 16 Sep 2016 16:36:23 +0200

tea (42.0.0-3) unstable; urgency=medium

  * Fix FTBFS: Use original name for icon.
    Thanks to Andreas Beckmann <anbe@debian.org> (Closes: #837086)

 -- Dr. Tobias Quathamer <toddy@debian.org>  Fri, 09 Sep 2016 11:34:53 +0200

tea (42.0.0-2) unstable; urgency=medium

  * Install tea icon into usr/share/icons/hicolor/128x128/apps
  * Remove obsolete .xpm icon
  * Add German translation to .desktop file

 -- Dr. Tobias Quathamer <toddy@debian.org>  Wed, 07 Sep 2016 16:46:11 +0200

tea (42.0.0-1) unstable; urgency=medium

  * Imported Upstream version 42.0.0
    - Drop patches with spelling fixes, have been applied upstream
  * Refresh remaining patch

 -- Dr. Tobias Quathamer <toddy@debian.org>  Thu, 11 Aug 2016 22:52:43 +0200

tea (41.1.1-1) unstable; urgency=medium

  * Imported Upstream version 41.1.1
  * Refresh patches
  * Update to Standards-Version 3.9.8, no changes needed

 -- Dr. Tobias Quathamer <toddy@debian.org>  Mon, 02 May 2016 21:07:22 +0200

tea (41.1.0-2) unstable; urgency=medium

  * Remove menu file
  * Build-Depend on qtbase5-dev instead of qt5-default
  * Enable all hardening features
  * Update Vcs-* URLs
  * Fix more spelling errors
  * Update to Standards-Version 3.9.7, no changes needed

 -- Dr. Tobias Quathamer <toddy@debian.org>  Tue, 09 Feb 2016 16:43:05 +0100

tea (41.1.0-1) unstable; urgency=medium

  * Imported Upstream version 41.1.0. Closes: #799769
  * Refresh patches

 -- Dr. Tobias Quathamer <toddy@debian.org>  Tue, 22 Sep 2015 19:53:14 +0200

tea (41.0.0-1) unstable; urgency=medium

  * Imported Upstream version 41.0.0
  * Update project homepage in debian/control
  * Update watch file
  * Switch to Qt5 for building the package

 -- Dr. Tobias Quathamer <toddy@debian.org>  Mon, 22 Jun 2015 09:33:26 +0200

tea (38.0.0-1) unstable; urgency=medium

  * Imported Upstream version 38.0.0
  * Refresh patches

 -- Tobias Quathamer <toddy@debian.org>  Wed, 22 Oct 2014 19:53:04 +0200

tea (37.2.1-1) unstable; urgency=low

  * Imported Upstream version 37.2.1
  * Update copyright years
  * Update to Standards-Version 3.9.6, no changes needed
  * Refresh patches

 -- Tobias Quathamer <toddy@debian.org>  Tue, 14 Oct 2014 12:27:18 +0200

tea (37.0.0-1) unstable; urgency=low

  * Imported Upstream version 37.0.0
  * Update watch file for new download location
  * Update URL of homepage in debian/control and debian/copyright
  * Add Keywords to .desktop file
  * Add new patch to fix installation path for binary
  * Reduce debian/rules and use even more debhelper magic
  * Fix another spelling error

 -- Tobias Quathamer <toddy@debian.org>  Thu, 31 Oct 2013 14:46:38 +0100

tea (36.0.2-1) unstable; urgency=low

  * Imported Upstream version 36.0.2. Closes: #719852
  * Change Recommends: from "aspell, hunspell" to "aspell | hunspell"
    Thanks to Justin B Rye <jbr@edlug.org.uk>
  * Package description review.
    Thanks to Justin B Rye <jbr@edlug.org.uk> (Closes: #680356)
  * Refresh patch to apply cleanly
  * Update debian/copyright
  * Update to Standards-Version 3.9.4, no changes needed

 -- Tobias Quathamer <toddy@debian.org>  Fri, 16 Aug 2013 21:19:56 +0200

tea (33.1.0-1) unstable; urgency=low

  * Imported Upstream version 33.1.0
  * Refresh patches

 -- Tobias Quathamer <toddy@debian.org>  Tue, 12 Jun 2012 17:06:59 +0200

tea (32.0.1-1) unstable; urgency=low

  * Imported Upstream version 32.0.1
  * Add packaging repository URL to debian/control
  * Use all hardening features
  * Refresh patches

 -- Tobias Quathamer <toddy@debian.org>  Sat, 24 Mar 2012 15:31:29 +0100

tea (31.2.0-1) unstable; urgency=low

  * Imported Upstream version 31.2.0
  * Refresh debian/patches
  * Switch to debhelper v9
  * Use machine-readable format v1.0 for debian/copyright
  * Update to Standards-Version 3.9.3

 -- Tobias Quathamer <toddy@debian.org>  Wed, 29 Feb 2012 13:05:53 +0100

tea (29.0.1-1) unstable; urgency=low

  * New upstream version
  * Update years in debian/copyright
  * Fix some more spelling errors
  * Update Standards-Version to 3.9.2, no changes needed

 -- Tobias Quathamer <toddy@debian.org>  Thu, 07 Apr 2011 20:23:45 +0200

tea (28.1.4-1) unstable; urgency=low

  * New upstream version
    - Add -lz to linking options. Closes: #556485
  * Add zlib1g-dev to Build-Depends
  * Use debhelper v8
  * Add patch to fix some spelling errors
  * Use -Wl,--as-needed to remove unnecessary linking to libraries

 -- Tobias Quathamer <toddy@debian.org>  Wed, 23 Feb 2011 01:30:35 +0100

tea (28.1.1-1) unstable; urgency=low

  * New upstream version
  * Update Standards-Version to 3.9.1, no changes needed

 -- Tobias Quathamer <toddy@debian.org>  Fri, 06 Aug 2010 16:42:36 +0200

tea (28.1.0-1) unstable; urgency=low

  * New upstream version
  * Update Standards-Version to 3.9.0, no changes needed

 -- Tobias Quathamer <toddy@debian.org>  Mon, 19 Jul 2010 10:58:34 +0200

tea (28.0.0-1) unstable; urgency=low

  * New upstream version
    - Add libx11-dev to Build-Dependencies (new screenshot feature)

 -- Tobias Quathamer <toddy@debian.org>  Tue, 15 Jun 2010 13:13:50 +0200

tea (27.0.0-1) unstable; urgency=low

  * New upstream version
    - German translation is now included, so those patches are removed.

 -- Tobias Quathamer <toddy@debian.org>  Fri, 05 Mar 2010 10:22:07 +0100

tea (26.2.2-1) unstable; urgency=low

  * New upstream version
  * Update Standards-Version to 3.8.4, no changes needed
  * Update copyright years

 -- Tobias Quathamer <toddy@debian.org>  Sat, 06 Feb 2010 18:33:06 +0100

tea (26.0.1-1) unstable; urgency=low

  * New maintainer (Closes: #533714)
  * New Upstream Version (Closes: #514878, LP: #327463)
    - No longer linked with OpenSSL library (Closes: #522313)
  * Add aspell and hunspell to recommends to enable spell-checking
    (Closes: #505493, LP: #292466)
  * Add a German translation
  * Switch to dpkg source format 3.0 (quilt)
  * Update Standards-Version to 3.8.3

 -- Tobias Quathamer <toddy@debian.org>  Thu, 12 Nov 2009 15:59:15 +0100

tea (17.6.1-1) unstable; urgency=low

  [ Marco Rodrigues ]
  * New upstream version.
  * Bump debhelper and compat to version 6.
  * Bump Standards-Version to 3.8.0.

  [ Lior Kaplan ]
  * Remove build dependency on autotools-dev as upstream updated it's
    config.{guess,sub}.

 -- Marco Rodrigues <gothicx@sapo.pt>  Mon, 09 Jun 2008 09:39:26 +0100

tea (17.6.0-1) unstable; urgency=low

  [ Marco Rodrigues ]
  * New upstream release
    - Depends on libcurl to support the new "Check for broken links" feature.

  [ Lior Kaplan ]
  * Fix tea-data's doc-base section as the doc-base manual changed.
  * Build against GtkSourceView2 instead of GtkSourceView.
  * Build depend on libzzip-dev, remove unzip from Recommends.
  * Build depend on autotools-dev as upsteam has an old confiug.sub file.
  * Remove old / unneeded build dependency on libgconf2-dev, libgnomevfs2-dev.

 -- Lior Kaplan <kaplan@debian.org>  Sat, 15 Mar 2008 12:09:13 +0200

tea (17.5.4-1) unstable; urgency=low

  * New upstream release

 -- Marco Rodrigues <gothicx@sapo.pt>  Thu, 31 Jan 2008 11:44:46 +0200

tea (17.4.1-2) unstable; urgency=low

  * Add configure option to enable unstripped binaries (Closes: #438111).
  * Add dh_desktop to debian/rules.
  * Bump Standards-Version to 3.7.3 (no changes needed).

 -- Marco Rodrigues <gothicx@sapo.pt>  Fri, 28 Dec 2007 21:53:36 +0200

tea (17.4.1-1) unstable; urgency=low

  * New upstream release
  * Add Homepage field to control file.
  * Add version and Removed Encoding from desktop file.

 -- Marco Rodrigues <gothicx@sapo.pt>  Fri, 30 Nov 2007 19:48:53 +0200

tea (17.3.0-1) unstable; urgency=low

  * New upstream release (Closes: #445709).

 -- Marco Rodrigues <gothicx@sapo.pt>  Sun, 07 Oct 2007 22:19:48 +0100

tea (17.0.1-1) unstable; urgency=low

  * New upstream release
    - Fixes FTBFS with libgtk2.0-dev 2.11.5 (Debian experimental and Ubuntu
      gutsy). Discovered by Jordan Mantha from Ubuntu.
    - Merges Debian patches upstream.

 -- Lior Kaplan <kaplan@debian.org>  Thu, 12 Jul 2007 01:49:22 +0300

tea (17.0.0-1) unstable; urgency=low

  * New upstream release
  * Update menu file as required by the menu transition.
  * Add .desktop file by Sam Cater (patch from Ubuntu).
  * Add João Pinto as a co-maintainer, as he worked on this package for
    getdeb.net (Closes: #408578).

 -- Lior Kaplan <kaplan@debian.org>  Tue, 10 Jul 2007 20:59:33 +0300

tea (17.0.0-1~getdeb1) feisty; urgency=low

  * New upstream version

 -- Joao Pinto <joao.pinto@getdeb.net>  Mon, 09 Jul 2007 21:16:57 +0000

tea (16.1.1-1~getdeb1) feisty; urgency=low

  * New upstream version

 -- Joao Pinto <joao.pinto@getdeb.net>  Tue, 01 May 2007 20:10:24 +0000

tea (16.1.0-0~getdeb1) edgy; urgency=low

  * New upstream version

 -- Joao Pinto <joao.pinto@getdeb.net>  Sun, 15 Apr 2007 16:28:44 +0000

tea (14.2.4-2ubuntu1) feisty; urgency=low

  * Added .desktop file (LP: #81456)

  * Changed Maintainer field [Jordan Mantha]

 -- Sam Cater <sacater@btopenworld.com>  Wed, 28 Feb 2007 20:53:39 +0000

tea (14.2.4-2) unstable; urgency=low

  * Fix debian/rules so the copyright file will be present in all binary
    packages (Closes: #393618).

 -- Lior Kaplan <kaplan@debian.org>  Fri, 20 Oct 2006 11:38:24 +0200

tea (14.2.4-1) unstable; urgency=low

  * New upstream release
    - Now tea quits cleanly (Closes: #368265)
    - Fix segfault trying to open a new file using "New Kwas" (Closes: #368254).
  * debian/control
    - Fix misleading description "Extremely small size" (Closes: #359776).
    - Upgrade to standards version 3.7.2 (no changes needed)

 -- Lior Kaplan <kaplan@debian.org>  Fri, 25 Aug 2006 20:06:40 +0300

tea (12.0-1) unstable; urgency=low

  * New upstream release
  * Dedicated to Shlomi Loubaton

 -- Lior Kaplan <kaplan@debian.org>  Sat, 18 Mar 2006 23:20:21 +0200

tea (11.0-1) unstable; urgency=low

  * New upstream release
  * Updated German translation by Tobias Toedter <t.toedter@gmx.net> (Closes: #346103)
  * debian/rules: Move dh_installdocs to arch-independent (Closes: #322767)
  * debian/copyright: fix missing part of the license ("or any later version").

 -- Lior Kaplan <webmaster@guides.co.il>  Fri,  6 Jan 2006 21:02:03 +0200

tea (10.1-1) unstable; urgency=low

  * New upstream release
  * debian/copyright: update author email.
  * teaed manpage updates. Closes: #308579, #308768, #311304
  * Upstream fix for changing the interface language on locale change.
    Closes: #311303
  * Upstream fix FTBFS on amd64/gcc-4.0. Closes: #314960
  * Split arch-independent files to tea-data package.

 -- Lior Kaplan <webmaster@guides.co.il>  Tue, 26 Jul 2005 22:55:05 +0300

tea (9.0-1) unstable; urgency=high

  * New upstream release
  * Split debian/tea.doc-base into two files. Closes: #304444
  * debian/rules: add --enable-debian to configure options.
  * Change /usr/bin/tea to /usr/bin/teaed (conflicts with dak package).
    Closes: #303352
  * Upstream applied patch for amd64/gcc-4.0. Closes: #303416
  * debian/control: add bzip2 & unzip to Recommends field.
  * remove debian/tea_spell.c.patch since fixed on upstream

 -- Lior Kaplan <webmaster@guides.co.il>  Mon,  9 May 2005 22:42:53 +0300

tea (7.0-2) unstable; urgency=low

  * src/tea_spell.c: Apply patch from Erik Johansson <erre@lysator.liu.se>.
    Closes: #303420
  * debian/control: add Shaul Karl <shaul@debian.org> to uploaders.
  * debian/tea.doc-base: add missing 'Files' field. Closes: #303305

 -- Lior Kaplan <webmaster@guides.co.il>  Sat,  9 Apr 2005 23:55:20 +0300

tea (7.0-1) unstable; urgency=low

  * Initial Release. Closes: #280900
  * Previous versions where uploaded to http://mentors.debian.net
  * New upstream release
  * debian/copyright: update upstream author email address
  * acknowledge a warning about 'missing' in build time - sent to upstream

 -- Lior Kaplan <webmaster@guides.co.il>  Wed, 16 Feb 2005 23:52:47 +0200
